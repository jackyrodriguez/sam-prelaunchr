FROM ruby:2.3.0
MAINTAINER jackelyn.rodriguez@acommerce.asia

RUN apt-get update -qq && apt-get install -y build-essential \
    wget curl \
    nodejs git git-core \
    zlib1g-dev libssl-dev \
    libreadline-dev libyaml-dev \
    libsqlite3-dev sqlite3 \
    libxml2-dev libxslt1-dev \
    libpq-dev


RUN gem install bundler && gem install debug_inspector -v 0.0.2 \
        && gem install nokogiri -v 1.6.8 && gem install byebug -v 9.0.5 \
        && gem install sqlite3 -v 1.3.11 && gem install binding_of_caller \
	&& gem install pg -v 0.18.4 && gem install puma -v 3.4.0

RUN mkdir /prelauncher_app
WORKDIR /prelauncher_app
ADD Gemfile /prelauncher_app/Gemfile
ADD Gemfile.lock /prelauncher_app/Gemfile.lock

RUN bundle install
ADD . /prelauncher_app
