class UserMailer < ActionMailer::Base
  default from: "Harry's <welcome@harrys.com>"

  def signup_email(user)
    @user = user
    @twitter_message = "Can't wait for @candidclothing.ph to launch! Give help just by signing up. #EthicalMadeEasy"

    mail(:to => user.email, :subject => "Thanks for signing up!")
  end
end
